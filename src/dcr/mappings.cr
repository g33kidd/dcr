# :nodoc:
def Array.new(pull : JSON::PullParser, client : Discord::Client)
  ary = new
  new(pull, client) do |element|
    ary << element
  end
  ary
end

# :nodoc:
def Array.new(pull : JSON::PullParser, client : Discord::Client)
  pull.read_array do
    yield T.new(pull, client)
  end
end

# :nodoc:
def Array.from_json(string_or_io, client : Discord::Client) : Nil
  parser = JSON::PullParser.new(string_or_io)
  new(parser, client) do |element|
    yield element, client
  end
  nil
end

# :nodoc:
def Object.from_json(string_or_io, client : Discord::Client) : self
  parser = JSON::PullParser.new(string_or_io)
  new parser, client
end

# :nodoc:
def Union.new(pull : JSON::PullParser, client : Discord::Client)
  # Optimization: use fast path for primitive types
  {% begin %}
    # Here we store types that are not primitive types
    {% non_primitives = [] of Nil %}

    {% for type, index in T %}
      {% if type == Nil %}
        return pull.read_null if pull.kind == :null
      {% elsif type == Bool ||
                 type == Int8 || type == Int16 || type == Int32 || type == Int64 ||
                 type == UInt8 || type == UInt16 || type == UInt32 || type == UInt64 ||
                 type == Float32 || type == Float64 ||
                 type == String %}
        value = pull.read?({{type}})
        return value unless value.nil?
      {% else %}
        {% non_primitives << type %}
      {% end %}
    {% end %}

    # If after traversing all the types we are left with just one
    # non-primitive type, we can parse it directly (no need to use `read_raw`)
    {% if non_primitives.size == 1 %}
      return {{non_primitives[0]}}.new(pull, client)
    {% end %}
  {% end %}

  string = pull.read_raw
  {% for type in T %}
    {% if type != Nil && type != Bool &&
            type != Int8 && type != Int16 && type != Int32 && type != Int64 &&
            type != UInt8 && type != UInt16 && type != UInt32 && type != UInt64 &&
            type != Float32 && type != Float64 &&
            type != String %}
      begin
        return {{type}}.from_json(string, client)
      rescue JSON::ParseException
        # Ignore
      end
    {% end %}
  {% end %}
  raise JSON::ParseException.new("Couldn't parse #{self} from #{string}", 0, 0)
end

module Discord
  # Interface for mentionable objects (users, channels, etc).
  #
  # Just `to_s` them (or `mention` if the object overrides `to_s`)
  # and you have a string that's a proper mention!
  module Mentionable
    abstract def mention(io : IO)
    abstract def id : UInt64

    # Returns a string that's a functional mention of this object.
    def mention
      String.build { |s| mention s }
    end

    class MentionParsingError < DCRError
      getter content : String

      def initialize(@content : String)
        super "Failed parsing mention: '#{@content}'."
      end
    end

    def to_s(io)
      mention io
    end
  end

  # Interface for objects which can be created from a mention.
  #
  # Just call `from_mention(String)` with the mention text to get an ID,
  # or call `from_mention(Client, String)` with the client and the
  # mention text to receive an actual object.
  module FromMentionable
    abstract def mention_prefixes : Tuple(String)
    abstract def resolve_from_id(client : Client, id : UInt64)

    # Parses the mention in *str* and returns an ID, or `nil` on failure.
    def from_mention(str : String) : UInt64?
      return nil if !str.starts_with?('<') || !str.ends_with?('>')
      str = str[1..-2]

      mention_prefixes.each do |pre|
        if str.starts_with? pre
          return str[pre.size..-1].to_u64?
        end
      end
      nil
    end

    # Parses the mention in *str* and returns an object, or `nil` on failure.
    def from_mention(client : Client, str : String) : self?
      from_mention(str).try { |id| resolve_from_id client, id }
    end

    # Parses the mention in *str* and returns an ID, or raises `MentionParsingError` on failure.
    def from_mention!(str : String) : UInt64
      from_mention(str) || raise MentionParsingError.new str
    end

    # Parses the mention in *str* and returns an object, or raises `MentionParsingError` on failure.
    def from_mention!(client : Client, str : String) : self
      from_mention(client, str) || raise MentionParsingError.new str
    end
  end

  module Payload
    include MagicJSON

    json_defaults getter: true

    module WithClientAccess
      include Payload

      # json_api_config input_method_name: "from_discord"
      field client : Client, extra_field: true
    end
  end
end

require "./converters"
require "./errors"
require "./mappings/*"
