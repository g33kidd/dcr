require "json"
require "time/format"
require "./mappings/channel"

module Discord
  DATE_FORMAT = Time::Format.new("%FT%T%:z")

  # :nodoc:
  module MaybeConverter(T)
    def self.from_json(parser : JSON::PullParser)
      parser.read_null_or { T.from_json parser }
    end

    def self.to_json(value, builder : JSON::Builder)
      value.to_json(builder)
    end
  end

  alias MaybeSnowflakeConverter = MaybeConverter(SnowflakeConverter)
  alias MaybeTimestampConverter = MaybeConverter(TimestampConverter)

  # :nodoc:
  module TimestampConverter
    SEND_FORMAT    = Time::Format.new("%FT%T%:z")
    RECEIVE_FORMAT = Time::Format.new("%FT%TZ")

    def self.from_json(parser : JSON::PullParser) : Time
      SEND_FORMAT.from_json(parser)
    end

    def self.to_json(value : Time, builder : JSON::Builder)
      RECEIVE_FORMAT.to_json(value.to_utc, builder)
    end
  end

  # :nodoc:
  module PresenceIDConverter
    def self.from_json(parser : JSON::PullParser) : UInt64
      id = nil
      parser.read_object do |k|
        if k == "id"
          id = parser.read_string.to_u64
        else
          parser.skip
        end
      end
      raise JSON::ParseException.new "PresenceIDConverter: missing attribute 'id'", 0, 0 if !id
      id
    end

    def self.to_json(value : UInt64, builder : JSON::Builder)
      value.to_json builder
    end
  end

  # :nodoc:
  module StringEnumConverter(T)
    def self.from_json(parser : JSON::PullParser) : T
      T.from_discord_s parser.read_string
    end

    def self.to_json(value : T, builder : JSON::Builder)
      value.to_discord_s builder
    end
  end

  # :nodoc:
  module SnowflakeConverter
    def self.from_json(parser : JSON::PullParser) : UInt64
      parser.read_string.to_u64
    end

    def self.to_json(value : UInt64, builder : JSON::Builder)
      builder.scalar(value.to_s)
    end
  end

  # :nodoc:
  module SnowflakeArrayConverter
    def self.from_json(parser : JSON::PullParser) : Array(UInt64)
      Array(String).new(parser).map &.to_u64
    end

    def self.to_json(value : Array(UInt64), builder : JSON::Builder)
      value.map(&.to_s).to_json(builder)
    end
  end

  # :nodoc:
  module TypedConverter(T)
    def self.from_json(parser : JSON::PullParser, client : Client) : Channel
      T.typed_from_json parser.read_raw, client
    end

    def self.to_json(value : T, builder : JSON::Builder)
      value.to_json(builder)
    end
  end

  # :nodoc:
  module TypedArrayConverter(T)
    def self.from_json(parser : JSON::PullParser, client : Client) : Array(T)
      ary = [] of T
      parser.read_array { ary << T.typed_from_json parser.read_raw, client }
      ary
    end

    def self.to_json(value : Array(T), builder : JSON::Builder)
      value.to_json(builder)
    end
  end

  # :nodoc:
  module ArrayConverter(T)
    def self.from_json(parser : JSON::PullParser, client : Client) : Array(T)
      ary = [] of T
      parser.read_array { ary << T.from_json parser.read_raw, client }
      ary
    end

    def self.to_json(value : Array(T), builder : JSON::Builder)
      value.to_json(builder)
    end
  end
end
