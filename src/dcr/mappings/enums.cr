module Discord
  @[Flags]
  enum Permissions : UInt64
    CreateInstantInvite = 1
    KickMembers         = 1 << 1
    BanMembers          = 1 << 2
    Administrator       = 1 << 3
    ManageChannels      = 1 << 4
    ManageGuild         = 1 << 5
    AddReactions        = 1 << 6
    ReadMessages        = 1 << 10
    SendMessages        = 1 << 11
    SendTTSMessages     = 1 << 12
    ManageMessages      = 1 << 13
    EmbedLinks          = 1 << 14
    AttachFiles         = 1 << 15
    ReadMessageHistory  = 1 << 16
    MentionEveryone     = 1 << 17
    UseExternalEmojis   = 1 << 18
    Connect             = 1 << 20
    Speak               = 1 << 21
    MuteMembers         = 1 << 22
    DeafenMembers       = 1 << 23
    MoveMembers         = 1 << 24
    UseVAD              = 1 << 25
    ChangeNickname      = 1 << 26
    ManageNicknames     = 1 << 27
    ManageRoles         = 1 << 28
    ManageWebhooks      = 1 << 29
    ManageEmojis        = 1 << 30

    def self.new(pull : JSON::PullParser)
      # see https://github.com/crystal-lang/crystal/issues/3448
      # #from_value errors
      Permissions.new(pull.read_int.to_u64)
    end
  end

  struct Message
    enum Type
      Normal
      RecipientAdded
      RecipientRemoved
      Call
      GroupNameChanged
      GroupIconChanged
      PinAdded
    end
  end

  struct PermissionOverwrite
    enum Type
      Role
      Member

      def self.from_discord_s(str : String)
        case str
        when "role"  ; Role
        when "member"; Member
        else           raise "Unknown permission overwrite type #{str}"
        end
      end

      def to_discord_s(io)
        case self
        when Role  ; io << "role"
        when Member; io << "member"
        else         io << to_s
        end
      end
    end
  end

  struct Presence
    enum Status
      Online
      Offline
      Idle
      DoNotDisturb
      Invisible

      def self.from_discord_s(str : String)
        case str
        when "online"   ; Online
        when "offline"  ; Offline
        when "idle"     ; Idle
        when "dnd"      ; DoNotDisturb
        when "invisible"; Invisible
        else              Online
        end
      end

      def to_discord_s(str : String)
        case self
        when Online      ; "online"
        when Offline     ; "offline"
        when Idle        ; "idle"
        when DoNotDisturb; "dnd"
        when Invisible   ; "invisible"
        else               "online"
        end
      end
    end
  end

  abstract struct Channel
    enum Type : UInt8
      GuildText
      PrivateDM
      GuildVoice
      PrivateGroup
    end
  end

  struct Guild
    enum VerificationLevel : UInt8
      None
      Low
      Medium
      TableFlip
      DoubleTableFlip
    end
  end
end
