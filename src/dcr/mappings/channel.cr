require "concurrent/channel"
require "./enums"
require "./user"

module Discord
  # A representation of a message in a Discord text channel.
  struct Message
    include Payload::WithClientAccess

    # :nodoc:
    def initialize(@client, message : Message, update : Gateway::MessageUpdatePayload)
      @id = message.@id
      @channel_id = message.@channel_id
      @type = update.@type || message.@type
      @content = update.@content || message.@content
      @author = update.@author || message.@author
      @timestamp = update.@timestamp || message.@timestamp
      @tts = update.@tts || message.@tts
      @mentions_everyone = update.@mentions_everyone || message.@mentions_everyone
      @mentions = update.@mentions || message.@mentions
      @mentioned_role_ids = update.@mentioned_role_ids || message.@mentioned_role_ids
      @attachments = update.@attachments || message.@attachments
      @embeds = update.@embeds || message.@embeds
      @pinned = update.@pinned || message.@pinned
    end

    field type : Message::Type
    field content : String
    field id : UInt64, converter: SnowflakeConverter
    field channel_id : UInt64, converter: SnowflakeConverter
    field author : User, pass_extra_fields: {:client}
    field timestamp : Time, converter: DATE_FORMAT
    field tts? : Bool, key: "tts"
    field mentions_everyone? : Bool, key: "mention_everyone"
    field mentions : Array(User), pass_extra_fields: {:client}
    field mentioned_role_ids : Array(UInt64), converter: SnowflakeArrayConverter, key: "mention_roles"
    field attachments : Array(Attachment)
    field embeds : Array(Embed)
    field pinned? : Bool = false, key: "pinned"

    def to_s(io)
      io << @content
    end

    def mentioned_roles
      @mentioned_role_ids.map { |id| @client.get_role id }
    end

    def channel
      @client.get_channel(@channel_id).as(TextChannel)
    end

    class NotInAGuildError < DCRError
      def initialize
        super "The channel this message has been sent in isn't in a guild."
      end
    end

    # Retrieves the `Guild` this message has been posted in, or return `nil`
    # if the channel this message has been posted in isn't in a guild.
    def guild : Guild?
      chan = channel
      if chan.is_a?(GuildChannel)
        return @client.get_guild chan.guild_id
      end
      nil
    end

    # Retrieves the `Guild` this message has been posted in. If the channel
    # this message has been posted in isn't in a guild, `NotInAGuildError`
    # will be raised.
    def guild! : Guild
      guild || raise NotInAGuildError.new
    end

    # Retrieves the `GuildMember` who sent this message. If the channel this
    # message has been posted in isn't in a guild, `nil` will be returned.
    def member : GuildMember?
      chan = channel
      if chan.is_a?(GuildChannel)
        return @client.get_guild_member guild_id: chan.guild_id, user_id: author.id
      end
      nil
    end

    # Retrieves the `GuildMember` who sent this message. If the channel
    # this message has been posted in isn't in a guild, `NotInAGuildError`
    # will be raised.
    def member! : GuildMember
      member || raise NotInAGuildError.new
    end

    # Sends a new message to the channel this message is from.
    def respond(content : String, embed : Embed? = nil)
      channel.send content, embed
    end

    # Attaches a file to the channel this message is from.
    def respond_with_file(path : String, content : String? = nil)
      channel.upload path, content
    end

    # Attaches a file to the channel this message is from, with a custom
    # filename and `IO` object.
    def respond_with_file(file : IO, filename : String? = nil, content : String? = nil)
      channel.upload file, filename, content
    end

    # Edits a message.
    #
    # This will raise `User::NotMeError` if you are not the author
    # of this message.
    def edit(content : String, embed : Embed? = nil)
      raise User::NotMeError.new if !author.me?

      @client.edit_message(
        channel_id: @channel_id,
        message_id: @id,
        content: content,
        embed: embed
      )
    end

    # Deletes a message.
    def delete
      @client.delete_message @channel_id
    end

    # Pins the message to the channel it was sent in.
    def pin
      channel.pin self
    end

    # Unpins the message from the channel it was sent in.
    def unpin
      channel.unpin self
    end

    # Adds a Unicode emoji reaction to the message.
    def react(emoji : Char)
      @client.create_reaction(
        channel_id: @channel_id,
        message_id: @id,
        emoji: emoji.to_s
      )
    end

    # Adds a custom emoji reaction to the message.
    def react(emoji : Emoji)
      @client.create_reaction(
        channel_id: @channel_id,
        message_id: @id,
        emoji: "#{emoji.name}:#{emoji.id}"
      )
    end

    # Removes the bot's Unicode emoji reaction from the message.
    def unreact(emoji : Char)
      @client.delete_own_reaction(
        channel_id: @channel_id,
        message_id: @id,
        emoji: emoji.to_s
      )
    end

    # Removes the bot's custom emoji reaction from the message.
    def unreact(emoji : Emoji)
      @client.delete_own_reaction(
        channel_id: @channel_id,
        message_id: @id,
        emoji: "#{emoji.name}:#{emoji.id}"
      )
    end

    # Removes a particular user's Unicode emoji reaction from the message.
    def unreact(user_or_member : User | GuildMember, emoji : Char)
      @client.delete_user_reaction(
        channel_id: @channel_id,
        message_id: @id,
        emoji: emoji.to_s,
        user_id: user_or_member.id
      )
    end

    # Removes a particular user's custom emoji reaction from the message.
    def unreact(user_or_member : User | GuildMember, emoji : Emoji)
      @client.delete_user_reaction(
        channel_id: @channel_id,
        message_id: @id,
        emoji: "#{emoji.name}:#{emoji.id}",
        user_id: user_or_member.id
      )
    end

    # Returns an array of `User`s who have reacted to this message
    # with the Unicode *emoji*.
    def reactors(emoji : Char)
      @client.get_reactions(
        channel_id: @channel_id,
        message_id: @id,
        emoji: emoji.to_s
      )
    end

    # Returns an array of `User`s who have reacted to this message
    # with the custom *emoji*.
    def reactors(emoji : Emoji)
      @client.get_reactions(
        channel_id: @channel_id,
        message_id: @id,
        emoji: "#{emoji.name}:#{emoji.id}"
      )
    end

    # Removes all reactions from the message.
    def unreact_all
      @client.delete_all_reactions(
        channel_id: @channel_id,
        message_id: @id
      )
    end
  end

  # Base type for all channel objects.
  abstract struct Channel
    include Payload::WithClientAccess
    include Mentionable
    extend FromMentionable

    abstract def id : UInt64
    abstract def name : String

    # Returns a representation of a channel based on the `type` field in
    # the JSON payload.
    def self.typed_from_json(data : String, client : Client) : self
      parser = JSON::PullParser.new data
      parser.read_object do |key|
        if key == "type"
          val = parser.read_int.to_u8
          case Channel::Type.new val
          when Channel::Type::GuildText
            return GuildTextChannel.from_json data, client
          when Channel::Type::PrivateDM
            return PrivateDMChannel.from_json data, client
          when Channel::Type::GuildVoice
            return GuildVoiceChannel.from_json data, client
          when Channel::Type::PrivateGroup
            return PrivateGroupChannel.from_json data, client
          else
            raise "Unknown channel type #{val}"
          end
        else
          parser.skip
        end
      end
      raise "Payload has no 'type' field"
    end

    def delete
      @client.delete_channel @id
    end

    protected def self.mention_prefixes
      {"@#"}
    end

    protected def self.resolve_from_id(client, id)
      client.get_channel id
    end

    def mention(io)
      io << "<#" << id << ">"
    end
  end

  # Mixin for private channels (DMs and groups).
  module PrivateChannel
    # Returns a representation of a private channel based on the `type`
    # field in the JSON payload.
    def self.typed_from_json(data : String, client : Client) : self
      parser = JSON::PullParser.new data
      parser.read_object do |key|
        if key == "type"
          val = parser.read_int.to_u8
          case Channel::Type.new val
          when Channel::Type::PrivateDM
            return PrivateDMChannel.from_json(data, client).as PrivateChannel
          when Channel::Type::PrivateGroup
            return PrivateGroupChannel.from_json(data, client).as PrivateChannel
          else
            raise "Unknown private channel type #{val}"
          end
        else
          parser.skip
        end
      end
      raise "Payload has no 'type' field"
    end
  end

  # Mixin for channels which have a real *name* field.
  module NamedChannel
    include Payload::WithClientAccess

    field name : String = ""

    def name=(name : String)
      @client.modify_channel channel_id: @id, name: name
      @name = name
    end
  end

  # Mixin for channels
  module GuildChannel
    include Payload::WithClientAccess
    include NamedChannel

    field guild_id : UInt64? = nil, getter: false, converter: MaybeSnowflakeConverter
    field permission_overwrites : Set(PermissionOverwrite)

    # Returns a representation of a guild channel based on the `type`
    # field in the JSON payload.
    def self.typed_from_json(data : String, client : Client) : self
      parser = JSON::PullParser.new data
      parser.read_object do |key|
        if key == "type"
          val = parser.read_int.to_u8
          case Channel::Type.new val
          when Channel::Type::GuildText
            return GuildTextChannel.from_json(data, client).as GuildChannel
          when Channel::Type::GuildVoice
            return GuildVoiceChannel.from_json(data, client).as GuildChannel
          else
            raise "Unknown guild channel type #{val}"
          end
        else
          parser.skip
        end
      end
      raise "Payload has no 'type' field"
    end

    # :nodoc:
    def guild_id=(id : UInt64)
      # guild_create channels don't have guild ids
      raise "Tried to set guild ID after it has already been set (note: this is an internal method!)" if !@guild_id.nil?
      @guild_id = id
    end

    def guild_id : UInt64
      @guild_id.as(UInt64)
    end

    def guild
      @client.get_guild guild_id
    end

    def position=(pos : UInt32)
      @client.modify_channel channel_id: @id, position: pos
      @position = pos
    end

    def set_permission_overwrite(member : GuildMember, allowed : Permissions? = nil, denied : Permissions? = nil) : Nil
      ovr = @permission_overwrites.find { |o| o.type == PermissionOverwrite::Type::Member && o.id == member }

      @client.edit_channel_permissions(
        channel_id: @id,
        overwrite_id: member.id,
        type: PermissionOverwrite::Type::Member,
        allow: allowed || ovr.try(&.allowed) || Permissions::None,
        deny: denied || ovr.try(&.denied) || Permissions::None
      )

      @permission_overwrites.delete ovr
      @permission_overwrites << PermissionOverwrite.new(
        type: PermissionOverwrite::Type::Member,
        id: member.id,
        allowed: allowed,
        denied: denied
      )
    end

    def set_permission_overwrite(role : Role, allow : Permissions? = nil, deny : Permissions? = nil) : Nil
      ovr = @permission_overwrites.find { |o| o.type == PermissionOverwrite::Type::Role && o.id == role }

      @client.edit_channel_permissions(
        channel_id: @id,
        overwrite_id: role.id,
        type: PermissionOverwrite::Type::Role,
        allow: allowed || ovr.try(&.allowed) || Permissions::None,
        deny: denied || ovr.try(&.denied) || Permissions::None
      )

      @permission_overwrites.delete ovr
      @permission_overwrites << PermissionOverwrite.new(
        type: PermissionOverwrite::Type::Role,
        id: role.id,
        allowed: allowed,
        denied: denied
      )
    end

    def remove_permission_overwrite(member_or_role : GuildMember | Role) : Nil
      type = member_or_role.is_a?(Role) ? PermissionOverwrite::Type::Role : PermissionOverwrite::Type::Member
      ovr = @permission_overwrites.find { |o| o.type == type && o.id == role }

      @client.delete_channel_permissions(
        channel_id: @id,
        overwrite_id: member_or_role.id
      )

      @permission_overwrites.delete ovr
    end

    def invite(max_age_seconds = 0_u32, max_uses = 0_u32, temporary = false) : InviteMetadata
      @client.create_channel_invite(
        channel_id: @id,
        max_age: max_age_seconds,
        max_uses: max_uses,
        temporary: temporary
      )
    end

    def invite(max_age : Time::Span, max_uses = 0_u32, temporary = false) : InviteMetadata
      invite span.seconds, max_uses, temporary
    end

    def invites : Array(Invite)
      @client.get_channel_invites @id
    end

    # Using the same logic as Discord itself, determines whether a member
    # (with all of their roles) has a certain permission on this channel.
    #
    # This method takes into account everything - the @everyone role,
    # normal role permission setup, per-channel role permission overwrites
    # and per-channel user permission overwrites.
    #
    # If you want to check specifically if the user has read or send
    # permissions, check out `can_read?` and `can_send_in?`.
    def member_has_permissions?(member : GuildMember, perms : Permissions) : Bool
      roles = member.roles.sort { |a, b| a.position <=> b.position }
      # in order from lowest positioned to highest positioned

      role_ovr_map = Hash(UInt64, PermissionOverwrite).new
      user_ovr = nil

      @permission_overwrites.each do |o|
        role_ovr_map[o.id] = o if o.type == PermissionOverwrite::Type::Role
        user_ovr = o if o.type == PermissionOverwrite::Type::Member && o.id == member.id
      end

      everyone_role = @client.get_role member.guild_id

      allowed = everyone_role.permissions

      roles.each do |r|
        allowed |= r.permissions
      end

      LOGGER.debug "Initial perms for member #{member.id} on #{member.guild_id}: #{allowed}"

      if ovr = role_ovr_map[everyone_role.id]?
        allowed |= ovr.allowed
        allowed &= ~ovr.denied
      end

      roles.each do |r|
        if ovr = role_ovr_map[r.id]?
          allowed |= ovr.allowed
          allowed &= ~ovr.denied

          LOGGER.debug "Perms after override #{r.name}: #{allowed}"
        end
      end

      if user_ovr
        allowed |= user_ovr.allowed
        allowed &= ~user_ovr.denied
      end

      LOGGER.debug "Final perms: #{allowed}"

      (allowed & perms) == perms
    end

    # Determines whether *member* can read messages in this channel.
    # (note: this doesn't apply to bots, which can always read messages)
    def member_can_read?(member : GuildMember) : Bool
      member_has_permissions? member, Permissions::ReadMessages
    end

    # Determines whether *member* can send messages in this channel.
    def member_can_send_in?(member : GuildMember)
      member_has_permissions? member, Permissions::SendMessages
    end
  end

  # Mixin for text channels.
  module TextChannel
    include Payload::WithClientAccess
    include MagicJSON

    # Returns a representation of a text channel based on the `type`
    # field in the JSON payload.
    def self.typed_from_json(data : String, client : Client) : TextChannel
      parser = JSON::PullParser.new data
      parser.read_object do |key|
        if key == "type"
          val = parser.read_int
          case val
          when 0
            return GuildTextChannel.from_json data, client
          when 1
            return PrivateDMChannel.from_json data, client
          when 3
            return PrivateGroupChannel.from_json data, client
          else
            raise "Unknown guild channel type #{val}"
          end
        else
          parser.skip
        end
      end
      raise "Payload has no 'type' field"
    end

    field last_message_id : UInt64? = nil, converter: MaybeSnowflakeConverter
    field last_pin_timestamp : Time? = nil, converter: MaybeTimestampConverter

    # :nodoc:
    def last_pin_timestamp=(time : Time)
      @last_pin_timestamp = time
    end

    class NoMessagesError < DCRError
      def initialize
        super "An empty array was passed to delete_messages!"
      end
    end

    # Helper method that takes in a veriable amount of arguments.
    # See `delete_messages`.
    def delete_messages(*msgs : Array(Message))
      delete_messages msgs
    end

    # Bulk deletes up to 100 messages no older than 2 weeks.
    #
    # This method will raise `NoMessagesError` if the array
    # is empty. If the array has one element, the standard
    # (single message) deletion method will be used. If the
    # array has at least 2 elements, the bulk delete endpoint
    # will be used.
    def delete_messages(msgs : Array(Message))
      raise NoMessagesError.new if msgs.size == 0
      if msg.size == 1
        @client.delete_message @id, msgs[0]
      else
        @client.bulk_delete_messages channel_id: @id, message_ids: msgs
      end
    end

    # Lists messages in this channel.
    #
    # The maximum *limit* possible is 1000.
    # You can use *before*, *after* or *around* to specify which messages
    # should be listed. Only one of these arguments should be provided
    # at once.
    def list_messages(limit : Int32 = 1000,
                      before : Message? = nil, after : Message? = nil,
                      around : Message? = nil) : Array(Message)
      @client.get_channel_messages(
        channel_id: @id,
        limit: limit,
        before: before.try(&.id),
        after: after.try(&.id),
        around: around.try(&.id)
      )
    end

    def pins : Array(Messages)
      @client.get_pinned_messages @id
    end

    # Pins *msg* in this channel.
    #
    # Note: *msg* must have been sent in this channel.
    def pin(msg : Message) : Nil
      @client.add_pinned_channel_message channel_id: @id, message_id: msg.id
    end

    # Unpins *msg* from this channel.
    #
    # Note: *msg* must have been sent in this channel.
    def unpin(msg : Message) : Nil
      @client.delete_pinned_channel_message channel_id: @id, message_id: msg.id
    end

    @typing_indicator_channel = ::Channel::Buffered(Bool).new capacity: 1
    @typing_indicator_fiber : Fiber? = nil

    # Starts a typing indicator that will show up for 10 seconds.
    #
    # If a message is sent in these 10 seconds, the typing indicator
    # will disappear.
    def indicate_typing : Nil
      @client.trigger_typing_indicator @id
    end

    # Attaches a file to this channel, with an optional message.
    def upload(path : String, content : String? = nil) : Nil
      File.open(path) { |f| upload f, File.basename(path), content }
    end

    # Attaches a file to this channel, with an optional message.
    #
    # This method gives you the option of providing a custom IO-compatible
    # object and a file name.
    def upload(file : IO, filename : String? = nil, content : String? = nil) : Nil
      @client.upload_file(
        channel_id: @id,
        content: content,
        file: file,
        filename: filename
      )
    end

    # Sends a message to the channel, with an optional embed.
    def send(content : String, embed : Embed? = nil)
      @client.create_message @id, content, embed
    end

    class NoMessagesError < DCRError
      def initialize
        super "This channel has no messages."
      end
    end

    class NoPinsError < DCRError
      def initialize
        super "This channel has no pins."
      end
    end

    # Returns the last message posted in this channel, or `nil` if
    # no messages have been posted in this channel.
    def last_message
      if @last_message_id
        @client.get_client_message @id, @last_message_id
      end
      nil
    end

    # Returns the last message posted in this channel. Raises
    # `NoMessagesError` if no messages have been posted in this
    # channel yet.
    def last_message!
      last_message || raise NoMessagesError.new
    end

    # Returns the timestamp of the last pinned message in this channel.
    # Raises `NoPinsError` if this channel has no pins.
    def last_pin_timestamp!
      last_pin_timestamp || raise NoPinsError.new
    end
  end

  # Representation of a text channel in a guild.
  struct GuildTextChannel < Channel
    include GuildChannel
    include TextChannel

    field id : UInt64, converter: SnowflakeConverter
    field topic : String = ""

    def topic=(topic : String)
      @client.modify_channel channel_id: @id, topic: topic
      @topic = topic
    end
  end

  # Representation of a private DM channel (one on one).
  struct PrivateDMChannel < Channel
    include TextChannel
    include PrivateChannel

    field id : UInt64, converter: SnowflakeConverter
    field recipients : Array(User), pass_extra_fields: {:client}, getter: false

    def name
      recipient.username
    end

    def recipient
      @recipients.first
    end
  end

  # Representation of a private group channel.
  struct PrivateGroupChannel < Channel
    include TextChannel
    include PrivateChannel
    include NamedChannel

    field id : UInt64, converter: SnowflakeConverter
    field recipients : Array(User), pass_extra_fields: {:client}
  end

  # Representation of a voice channel in a guild.
  struct GuildVoiceChannel < Channel
    include GuildChannel
    include NamedChannel

    field id : UInt64, converter: SnowflakeConverter

    field bitrate : UInt32
    field user_limit : UInt32? = nil

    def has_user_limit?
      !@user_limit.nil?
    end

    def bitrate=(bitrate : UInt32)
      @client.modify_channel channel_id: @id, bitrate: bitrate
      @bitrate = bitrate
    end

    def user_limit=(user_limit : UInt32)
      @client.modify_channel channel_id: @id, user_limit: user_limit
      @user_limit = user_limit
    end
  end

  # Per-channel permission overwrite.
  struct PermissionOverwrite
    include Payload

    # :nodoc:
    def initialize(@type, @id, @allowed, @denied)
    end

    field id : UInt64, converter: SnowflakeConverter
    field type : PermissionOverwrite::Type, converter: StringEnumConverter(PermissionOverwrite::Type)
    field allowed : Permissions, key: "allow"
    field denied : Permissions, key: "deny"

    def allows?(perms : Permissions) : Bool
      allowed === perms
    end

    def denies?(perms : Permissions) : Bool
      denied === perms
    end

    # Returns true if *perms* is neither denied nor allowed in this overwrite.
    def unspecified?(perms : Permissions) : Bool
      !allows?(perms) && !denies?(perms)
    end
  end

  struct Reaction
    include Payload

    field user_id : UInt64, converter: SnowflakeConverter
    field channel_id : UInt64, converter: SnowflakeConverter
    field message_id : UInt64, converter: SnowflakeConverter
    field emoji : ReactionEmoji
  end

  struct ReactionEmoji
    include Payload

    field id : UInt64? = nil, converter: MaybeSnowflakeConverter
    field name : String

    def custom?
      !@id.nil?
    end
  end

  struct Embed
    include Payload

    def initialize(@title : String? = nil, @type : String = "rich",
                   @description : String? = nil, @url : String? = nil,
                   @timestamp : Time? = nil, @color : UInt32? = nil,
                   @footer : EmbedFooter? = nil, @image : EmbedImage? = nil,
                   @thumbnail : EmbedThumbnail? = nil, @author : EmbedAuthor? = nil,
                   @fields : Array(EmbedField)? = nil)
    end

    field type : String
    field title : String? = nil
    field description : String? = nil
    field url : String? = nil
    field timestamp : Time? = nil, converter: TimestampConverter
    field color : UInt32? = nil
    field footer : EmbedFooter? = nil
    field image : EmbedImage? = nil
    field thumbnail : EmbedThumbnail? = nil
    field video : EmbedVideo? = nil
    field provider : EmbedProvider? = nil
    field author : EmbedAuthor? = nil
    field fields : Array(EmbedField)? = nil

    {% unless flag?(:murica) %}
      def colour
        color
      end
    {% end %}
  end

  struct EmbedThumbnail
    include Payload

    def initialize(@url : String)
    end

    field url : String
    field proxy_url : String? = nil
    field height : UInt32? = nil
    field width : UInt32? = nil
  end

  struct EmbedVideo
    include Payload

    field url : String
    field height : UInt32
    field width : UInt32
  end

  struct EmbedImage
    include Payload

    def initialize(@url : String)
    end

    field url : String
    field proxy_url : String? = nil
    field height : UInt32? = nil
    field width : UInt32? = nil
  end

  struct EmbedProvider
    include Payload

    field name : String
    field url : String? = nil
  end

  struct EmbedAuthor
    include Payload

    def initialize(@name : String? = nil, @url : String? = nil, @icon_url : String? = nil)
    end

    field name : String? = nil
    field url : String? = nil
    field icon_url : String? = nil
    field proxy_icon_url : String? = nil
  end

  struct EmbedFooter
    include Payload

    def initialize(@text : String? = nil, @icon_url : String? = nil)
    end

    field text : String? = nil
    field icon_url : String? = nil
    field proxy_icon_url : String? = nil
  end

  struct EmbedField
    include Payload

    def initialize(@name : String, @value : String, @inline : Bool = false)
    end

    field name : String
    field value : String
    field inline? : Bool, key: "inline"
  end

  struct Attachment
    include Payload

    field id : UInt64, converter: SnowflakeConverter
    field filename : String
    field size : UInt32
    field url : String
    field proxy_url : String
    field height : UInt32? = nil
    field width : UInt32? = nil
  end
end
