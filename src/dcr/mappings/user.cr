module Discord
  struct User
    include Payload::WithClientAccess
    include Mentionable
    extend FromMentionable

    enum DefaultAvatar
      Blurple
      Grey
      Green
      Yellow
      Red

      def max
        4
      end

      def url
        case self
        when Blurple
          "https://discordapp.com/assets/6debd47ed13483642cf09e832ed0bc1b.png"
        when Grey
          "https://discordapp.com/assets/322c936a8c8be1b803cd94861bdfa868.png"
        when Green
          "https://discordapp.com/assets/dd4dbc0016779df1378e7812eabaa04d.png"
        when Yellow
          "https://discordapp.com/assets/0e291f67c9274a1abdddeb3fd919cbaa.png"
        when Red
          "https://discordapp.com/assets/1cbd08c76f8af6dddce02c5138971129.png"
        else
          raise "Unknown default avatar type #{self}"
        end
      end
    end

    # :nodoc:
    def initialize(@client : Client, partial : PartialUser)
      @username = partial.@username.not_nil!
      @id = partial.@id
      @discriminator = partial.@discriminator.not_nil!
      @avatar = partial.@avatar
      @email = partial.@email
      @bot = partial.@bot
    end

    field username : String
    field id : UInt64, converter: SnowflakeConverter
    field discriminator : String
    field avatar : String? = nil, getter: false
    field email : String? = nil
    field bot? : Bool = false, key: "bot"

    def me?
      @id == @client.get_current_user.id
    end

    class NotMeError < DCRError
      def initialize
        super "This user is not me."
      end
    end

    # Sets your username.
    #
    # Raises `NotMeError` if this user isn't you.
    def username=(username : String)
      raise NotMeError.new if !me?
      @client.modify_current_user username: username
      @username = username
    end

    # Sets your avatar.
    #
    # Raises `NotMeError` if this user isn't you.
    def avatar=(url : String)
      raise NotMeError.new if !me?
      @client.modify_current_user avatar: url
      @avatar = url
    end

    # Sets your avatar.
    #
    # Raises `NotMeError` if this user isn't you.
    def avatar=(avatar : DefaultAvatar)
      self.avatar = avatar.url
    end

    # Sets your status.
    #
    # Raises `NotMeError` if this user isn't you.
    def status=(status : Status)
      raise NotMeError.new if !me?
      @client.status_update status: statusaa
      @status = status
    end

    # Sets your game.
    #
    # Raises `NotMeError` if this user isn't you.
    def game=(game : GamePlaying)
      raise NotMeError.new if !me?
      @client.status_update game: game
      @game = game
    end

    def dm
      @client.create_dm @id
    end

    def dm(content : String, embed : Embed? = nil)
      chan = dm
      chan.send content, embed
      chan
    end

    def dm_file(path : String, content : String? = nil)
      chan = dm
      chan.upload path, content
      chan
    end

    def dm_file(file : IO, filename : String? = nil, content : String? = nil)
      chan = dm
      chan.upload file, filename, content
      chan
    end

    def game
      @client.get_presence(@id).game
    end

    def status
      @client.get_presence(@id).status
    end

    # Determines whether the user has the default Discord logo avatar.
    def default_avatar? : Bool
      @avatar.nil?
    end

    # Returns a URL pointing to the avatar.
    #
    # Note: this will return a URL even for default avatars.
    def avatar : String
      if _avatar = @avatar
        _avatar
      else
        DefaultAvatar.new(@discriminator.to_i % DefaultAvatar.max).url
      end
    end

    # Retrieves a `GuildMember` representing this user as a member of *guild*.
    def on(guild : Guild)
      @client.get_guild_member user_id: id, guild_id: guild.id
    end

    # Determines whether this user is banned from *guild*.
    def banned_from?(guild : Guild)
      @client.get_guild_bans(guild.id).any? &.id == guild.id
    end

    # Unbans this user from *guild*.
    def unban_from(guild : Guild)
      @client.remove_guild_ban guild.id, @id
    end

    # Determines if this is a system (fake) account.
    #
    # System accounts are used for example in messages about new pins
    # or in webhooks.
    def system?
      discriminator == "0000"
    end

    protected def self.mention_prefixes
      {"@!", "@"}
    end

    protected def self.resolve_from_id(client, id)
      client.get_user id
    end

    def mention(io)
      io << "<@!" << id << ">"
    end
  end

  struct UserGuild
    include Payload

    field id : UInt64, converter: SnowflakeConverter
    field name : String
    field icon : String
    field owner? : Bool, key: "owner"
    field permissions : Permissions
  end

  struct Connection
    include Payload

    field id : UInt64, converter: SnowflakeConverter
    field name : String
    field type : String
    field revoked? : Bool, key: "revoked"
  end

  struct GamePlaying
    include Payload

    def initialize(@name : String? = nil, @type : Int64 | String | Nil = nil, @url : String? = nil)
    end

    field name : String? = nil
    field type : Int64 | String | Nil # TODO wtf
    field url : String? = nil
  end

  struct Presence
    include Payload::WithClientAccess

    # :nodoc:
    def initialize(@client, update : Gateway::PresenceUpdatePayload)
      @id = update.id
      @game = update.game
      @status = update.status
    end

    # :nodoc:
    def initialize(@client, @id, @game, @status)
    end

    field id : UInt64, key: "user", converter: PresenceIDConverter
    field game : GamePlaying? = nil
    field status : Presence::Status, converter: StringEnumConverter(Presence::Status)
  end
end
