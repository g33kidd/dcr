require "./user"

module Discord
  module InviteCommon
    include Payload::WithClientAccess

    field code : String
    field guild : InviteGuild
    field channel : InviteChannel

    def delete
      @client.delete_invite @code
    end
  end

  struct Invite
    include InviteCommon

    def self.get(code : String)
      @client.get_invite code
    end
  end

  struct InviteMetadata
    include InviteCommon

    field inviter : User
    field users : UInt32
    field max_uses : UInt32
    field max_age : UInt32
    field temporary? : Bool, key: "temporary"
    field created_at : Time, converter: Time::Format::ISO_8601_DATE
    field revoked? : Bool, key: "revoked"
  end

  struct InviteGuild
    include Payload

    field id : UInt64, converter: SnowflakeConverter
    field name : String
    field splash_hash : String? = nil
  end

  struct InviteChannel
    include Payload

    field id : UInt64, converter: SnowflakeConverter
    field name : String
    field type : UInt8
  end
end
