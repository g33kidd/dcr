# multicommand.cr is an example that uses a simple command "dispatcher"
# via a case statement.
# This example features a few commands:
# » !help        ==> sends a dm (direct message) to the user
#                    with information
# » !about       ==> prints about information in a code block
# » !echo <args> ==> echos args
# » !date        ==> prints the current date

require "../src/dcr"

# Set the DCR_TOKEN environment variable before running this script, or just change this:
client = Discord::CachedClient.new "Bot #{ENV["DCR_TOKEN"]}"

# Command Prefix
PREFIX = "!"

client.on_message_create do |msg|
  next if !msg.content.starts_with? PREFIX
  split = msg.content.split ' '

  command = split[0][PREFIX.size..-1]
  if split.size == 1
    args = [] of String
  else
    args = split[1..-1]
  end

  case command
  when "help"
    msg.author.dm "Help is on the way!"
  when "about"
    msg.respond "```\nBot developed by discordcr\n```"
  when "echo"
    msg.respond args.join " "
  when "date"
    msg.respond Time.now.to_s("%D")
  when "test"
    msg.respond Discord::User.from_mention(client, args[0]).to_s
  end
end

client.run
